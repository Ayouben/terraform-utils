# Pre-commit checker for terraform

## Install prerequisites for pre-commit

Follow instructions here to install all the prerequisites :
- https://github.com/gruntwork-io/pre-commit
- https://github.com/antonbabenko/pre-commit-


## Usage

Create file `.pre-commit-config.yaml` in your terraform project

```yaml
fail_fast: false

repos:
  - repo: https://gitlab.com/keltiotechnology/terraform/terraform-utils
    rev: "v0.1"
    hooks:
      - id: terraform_unused_vars
        files: \.tf$
        exclude: \.+.terraform\/.*$

      - id: terraform-validate
      - id: tflint
      - id: terraform_fmt
      - id: checkov
      - id: tfsec
        args: ['terraform.tfvars']
      - id: infracost
      - id: terraform_docs
      - id: markdown-link-check

```

You can set fail_fast to true so that pre-commit will stop immediately after the first hook fails.

Install the pre-commit script

```bash
pre-commit install
```

Run git commit or run this command:

```bash
pre-commit run -a
```

If we want to skip pre-commit:

```bash
git commit -m "my msg" --no-verify
```

## Notes

- For terraform-docs, your readme must be named "README.md" and have these lines:

```markdown
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Infracost
<!-- BEGINNING OF PRE-COMMIT-INFRACOST_MARKER HOOK -->
<!-- END OF PRE-COMMIT-INFRACOST_MARKER HOOK -->
```

Please see folder terraform_docs_sample_config for sample README.md and terraform-docs configuration files.
You may want to check more about terraform-docs configuration [here](https://github.com/terraform-docs/terraform-docs/blob/master/docs/user-guide/configuration.md).

## Other tools cool to use

AWS Vault        : https://github.com/99designs/aws-vault
AWS switch roles : https://chrome.google.com/webstore/detail/aws-extend-switch-roles/jpmkfafbacpgapdghgdpembnojdlgkdl
