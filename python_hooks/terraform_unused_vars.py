#!/usr/bin/python
import sys
from os import path
import argparse
import subprocess
import json
import re
import glob

def getAllVars():
    res = subprocess.run(['terraform-docs', 'json', '.'],
                         stdout=subprocess.PIPE).stdout.decode('utf-8')
    data = json.loads(res)['inputs']

    all_used_variables = []

    for used_variable in data:
        all_used_variables.append(used_variable['name'])

    return all_used_variables

def getUsedVariables(file):
    """
    Syntax Variable input defined here: https://www.terraform.io/docs/language/syntax/configuration.html
    Identifiers can contain letters, digits, underscores (_), and hyphens (-). 
    The first character of an identifier must not be a digit, to avoid ambiguity with literal numbe
    """
    pattern = r"\{?var\.[\w-]+\}?"

    all_used_vars = []
    tf_file = open(file, 'r')
    count = 0

    while True:
        count += 1
        line = tf_file.readline()

        if not line:
            break

        m = re.findall(pattern, line.strip())
        for v in m:
            d = v.replace("var.", "").replace("{", "").replace("}", "")
            all_used_vars.append(d)

    tf_file.close()

    return all_used_vars


def _main(files):
    all_used_variables = set(getAllVars())
    all_declared_variables = []
    for f in files:
        all_declared_variables += getUsedVariables(f)

    all_declared_variables = set(all_declared_variables)

    unused_variables = all_used_variables.difference(all_declared_variables)

    print("Unused variables: ")
    if len(unused_variables) == 0:
        print("No unused variable found!")
        exit(0)
    else:
        print(
            f"Found {len(unused_variables)} unused variables in your module: ")
        for v in unused_variables:
            print("- ", v)
        exit(1)

def main():
    tf_files = sys.argv[1:]

    for read_input in tf_files:
        if not path.exists(read_input):
            msg = f"File not found: {read_input}"
            print(msg)
            exit(-1)
    if len(tf_files) == 0:
        print("No terraform files provided as input")
        tf_files = set(glob.glob("*.tf"))
        excluded_files = set(['variables.tf', 'outputs.tf'])
        tf_files = tf_files.difference(excluded_files)
        print("Use these files as input: ", tf_files)

    _main(tf_files)

if __name__ == '__main__':
    sys.exit(main())