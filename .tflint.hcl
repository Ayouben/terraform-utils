plugin "aws" {
  enabled = true
  version = "0.6.0"
  source  = "github.com/terraform-linters/tflint-ruleset-aws"
}

rule "terraform_workspace_remote" {
  enabled = false
}
